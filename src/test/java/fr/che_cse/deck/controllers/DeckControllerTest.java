package fr.che_cse.deck.controllers;

import fr.che_cse.deck.utils.URL;
import org.json.JSONObject;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertNotSame;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class DeckControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void test1_newDeck() throws Exception {

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(URL.DECK_BASE + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.post(URL.DECK_BASE)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject deck = new JSONObject(mvcResult.getResponse().getContentAsString());

        assertTrue(deck.has("id"));
        assertEquals(1, deck.get("id"));
        assertTrue(deck.has("cards"));
        assertEquals(52, deck.getJSONArray("cards").length());
    }

    @Test
    public void test2_shuffleDeck() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(URL.DECK_BASE + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject deckBeforeShuffle = new JSONObject(mvcResult.getResponse().getContentAsString());

        mvcResult = mvc.perform(MockMvcRequestBuilders.post(URL.DECK_BASE + URL.DECK_SHUFFLE + "?id=0")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.post(URL.DECK_BASE + URL.DECK_SHUFFLE + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject deck = new JSONObject(mvcResult.getResponse().getContentAsString());

        assertTrue(deck.has("id"));
        assertEquals(1, deck.get("id"));
        assertTrue(deck.has("cards"));
        assertEquals(52, deck.getJSONArray("cards").length());

        assertEquals(deckBeforeShuffle.getJSONArray("cards").length(), deck.getJSONArray("cards").length());
        assertNotSame(deckBeforeShuffle.getJSONArray("cards"), deck.getJSONArray("cards"));
    }

    @Test
    public void test3_getCards() throws Exception {

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(URL.DECK_BASE + URL.DECK_CARDS + "?id=0")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.post(URL.DECK_BASE + URL.DECK_CARDS + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject card = new JSONObject(mvcResult.getResponse().getContentAsString());

        assertEquals(2, card.length());
        assertTrue(card.has("color"));
        assertTrue(card.has("value"));

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(URL.DECK_BASE + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject deck = new JSONObject(mvcResult.getResponse().getContentAsString());

        assertTrue(deck.has("cards"));
        assertEquals(51, deck.getJSONArray("cards").length());
    }

    @Test
    public void test4_removeDeck() throws Exception {

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(URL.DECK_BASE + "?id=0")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.delete(URL.DECK_BASE + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject response = new JSONObject(mvcResult.getResponse().getContentAsString());

        assertTrue(response.has("bool"));
        assertEquals(true, response.get("bool"));
    }

}