package fr.che_cse.historic.controllers;

import fr.che_cse.historic.utils.URL;
import org.json.JSONObject;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static junit.framework.TestCase.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class HistoricControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void test1_newHistoric() throws Exception {

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(URL.HISTORIC_BASE + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(400, mvcResult.getResponse().getStatus());

        mvcResult = mvc.perform(MockMvcRequestBuilders.post(URL.HISTORIC_BASE)
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject historic = new JSONObject(mvcResult.getResponse().getContentAsString());

        assertTrue(historic.has("id"));
        assertEquals(1, historic.get("id"));
        assertTrue(historic.has("date"));
        assertTrue(historic.has("actions"));
    }

    @Test
    public void test2_AddAction() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(URL.HISTORIC_BASE + URL.HISTORIC_ACTIONS + "?id=1").content("oui")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject action = new JSONObject(mvcResult.getResponse().getContentAsString());

        assertTrue(action.has("date"));
        assertTrue(action.has("body"));
        assertEquals("oui", action.getString("body"));

        mvcResult = mvc.perform(MockMvcRequestBuilders.get(URL.HISTORIC_BASE + "?id=1")
                .accept(APPLICATION_JSON)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());

        JSONObject historic = new JSONObject(mvcResult.getResponse().getContentAsString());

        assertTrue(historic.has("id"));
        assertEquals(1, historic.get("id"));
        assertTrue(historic.has("date"));
        assertTrue(historic.has("actions"));
        assertEquals(1, historic.getJSONArray("actions").length());
    }

}