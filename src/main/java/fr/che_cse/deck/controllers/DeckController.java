package fr.che_cse.deck.controllers;

import fr.che_cse.deck.models.Card;
import fr.che_cse.deck.models.Deck;
import fr.che_cse.deck.services.DeckService;
import fr.che_cse.deck.utils.URL;
import fr.che_cse.deck.utils.exceptions.BadRequestException;
import fr.che_cse.deck.utils.responses.BooleanResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(URL.DECK_BASE)
public class DeckController extends GenericController {

    @Autowired
    private DeckService deckService;

    @PostMapping
    public Deck createDeck() {
        return deckService.createDeck();
    }

    @GetMapping
    public Deck getDeck(@RequestParam String id) throws BadRequestException {
        return deckService.findDeck(id);
    }

    @DeleteMapping
    public BooleanResponse deleteDeck(@RequestParam String id) throws BadRequestException {
        return new BooleanResponse(deckService.removeDeck(id));
    }

    @PostMapping(URL.DECK_SHUFFLE)
    public Deck shuffleDeck(@RequestParam String id) throws BadRequestException {
        return deckService.shuffleDeck(id);
    }

    @PostMapping(URL.DECK_CARDS)
    public Card getCard(@RequestParam String id) throws BadRequestException {
        return deckService.getCardFromDeck(id);
    }
}

