package fr.che_cse.deck.utils.responses;

public class BooleanResponse {
    public boolean bool;

    public BooleanResponse(boolean b) {
        bool = b;
    }
}
