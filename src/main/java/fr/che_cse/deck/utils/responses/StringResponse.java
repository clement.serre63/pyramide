package fr.che_cse.deck.utils.responses;

public class StringResponse {
    public String message;

    public StringResponse(Long s){
        message = String.valueOf(s);
    }

    public StringResponse(String s){
        message = s;
    }
}
