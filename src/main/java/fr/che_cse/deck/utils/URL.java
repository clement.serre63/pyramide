package fr.che_cse.deck.utils;

public class URL {
    public static final String DECK_BASE = "/api/decks";
    public static final String DECK_SHUFFLE = "/shuffle";
    public static final String DECK_CARDS = "/cards";
}
