package fr.che_cse.deck.utils;

public enum CardColor {
    CARREAU("carreau"),
    COEUR("coeur"),
    PIQUE("pique"),
    TREFLE("trefle");

    private String color;

    CardColor(String color) {
        this.color = color;
    }

    public String toString() {
        return color;
    }
}
