package fr.che_cse.deck.services;

import fr.che_cse.deck.models.Card;
import fr.che_cse.deck.models.Deck;
import fr.che_cse.deck.repositories.CardRepository;
import fr.che_cse.deck.repositories.DeckRepository;
import fr.che_cse.deck.utils.CardColor;
import fr.che_cse.deck.utils.exceptions.BadRequestException;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Optional;

@Service
@Transactional
public class DeckService {

    @Autowired
    private DeckRepository deckRepository;

    @Autowired
    private CardRepository cardRepository;

    /**
     * Create a new deck in DB.
     *
     * @return the created deck
     */
    public Deck createDeck() {
        Deck deck = new Deck();
        Card card;

        initDeckFromDB(deck);

        for (CardColor cardColor : CardColor.values()) {
            for (int i = 1; i < 14; i++) {
                card = new Card(cardColor.toString(), i);

                cardRepository.save(card);

                deck.cards.add(card);
            }
        }

        deckRepository.save(deck);

        return deck;
    }

    public Deck findDeck(@NotNull String id) throws BadRequestException {
        Optional<Deck> optionalDeck = deckRepository.findById(Long.valueOf(id));

        if (!optionalDeck.isPresent()) {
            throw new BadRequestException("Deck not found.");
        }

        return optionalDeck.get();
    }

    private void initDeckFromDB(@NotNull Deck deck) {
        Hibernate.initialize(deck.cards);
        if (deck.cards == null) {
            deck.cards = new ArrayList<>();
        }
    }

    public Deck shuffleDeck(String id) throws BadRequestException {
        return shuffleDeck(findDeck(id));
    }

    public Deck shuffleDeck(Deck deck) {
        deck.shuffle();

        deckRepository.save(deck);

        return deck;
    }

    public Card getCardFromDeck(@NotNull String id) throws BadRequestException {
        return getCardFromDeck(findDeck(id));
    }

    public Card getCardFromDeck(@NotNull Deck deck) throws BadRequestException {
        if (deck.cards.isEmpty()) {
            throw new BadRequestException("Deck is empty.");
        }

        Card card = deck.cards.get(0);
        deck.cards.remove(0);

        cardRepository.delete(card);
        deckRepository.save(deck);

        return card;
    }

    public boolean removeDeck(@NotNull String id) throws BadRequestException {
        Deck deck = findDeck(id);

        for (Card card : deck.cards) {
            cardRepository.delete(card);
        }

        deckRepository.delete(deck);

        return true;
    }
}
