package fr.che_cse.deck.models;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

@Proxy(lazy = false)
@Entity
@Table(name = "decks")
public class Deck {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    public Long id;

    @OneToMany
    public List<Card> cards;

    public Deck() {
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }

}
