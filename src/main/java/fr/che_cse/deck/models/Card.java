package fr.che_cse.deck.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "cards")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    public Long id;

    @Column
    public String color;

    @Column
    public int value;

    @ManyToOne
    @JsonIgnore
    public Deck deck;

    public Card() {
        this("", 0);
    }

    public Card(String color, int value) {
        this.color = color;
        this.value = value;
    }
}
