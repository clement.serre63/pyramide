package fr.che_cse.deck;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "deckEntityManager",
        transactionManagerRef = "deckTransactionManager",
        basePackages = "fr.che_cse.deck"
)
public class DeckDBConfig {

    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.deck.datasource")
    public DataSource deckDataSource() {
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
    }

    @Primary
    @Bean
    public JpaVendorAdapter deckJpaVendorAdapter() {
        HibernateJpaVendorAdapter bean = new HibernateJpaVendorAdapter();
        bean.setDatabase(Database.H2);
        bean.setGenerateDdl(true);
        bean.setShowSql(true);

        return bean;
    }

    @Primary
    @Bean(name = "deckEntityManager")
    public LocalContainerEntityManagerFactoryBean deckEntityManagerFactory(DataSource deckDataSource,
                                                                       JpaVendorAdapter deckJpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(deckDataSource);
        bean.setJpaVendorAdapter(deckJpaVendorAdapter);
        bean.setPackagesToScan("fr.che_cse.deck");

        return bean;
    }

    @Primary
    @Bean(name = "deckTransactionManager")
    public PlatformTransactionManager deckTransactionManager(@Qualifier("deckEntityManager") EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }
}
