package fr.che_cse.historic.controllers;

import fr.che_cse.historic.utils.exceptions.BadRequestException;
import fr.che_cse.historic.utils.responses.StringResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class GenericController {
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    StringResponse handleBadRequestException(BadRequestException e){
        return new StringResponse(e.getMessage());
    }
}
