package fr.che_cse.historic.controllers;

import fr.che_cse.historic.models.Action;
import fr.che_cse.historic.models.Historic;
import fr.che_cse.historic.services.HistoricService;
import fr.che_cse.historic.utils.URL;
import fr.che_cse.historic.utils.exceptions.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(URL.HISTORIC_BASE)
public class HistoricController extends GenericController {
    @Autowired
    private HistoricService historicService;

    @PostMapping
    public Historic createSession() {
        return historicService.createSession();
    }

    @PostMapping(URL.HISTORIC_ACTIONS)
    public Action addAction(@RequestParam String id, @RequestBody String body) throws BadRequestException {
        return historicService.addActionToHistoric(id, body);
    }

    @GetMapping
    public Historic getHistoric(@RequestParam String id) throws BadRequestException {
        return historicService.findHistoric(id);
    }

}
