package fr.che_cse.historic;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "historicEntityManager",
        transactionManagerRef = "historicTransactionManager",
        basePackages = "fr.che_cse.historic"
)
public class HistoricDBConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.historic.datasource")
    public DataSource historicDataSource() {
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
    }

    @Bean
    public JpaVendorAdapter historicJpaVendorAdapter() {
        HibernateJpaVendorAdapter bean = new HibernateJpaVendorAdapter();
        bean.setDatabase(Database.H2);
        bean.setGenerateDdl(true);
        bean.setShowSql(true);

        return bean;
    }

    @Bean(name = "historicEntityManager")
    public LocalContainerEntityManagerFactoryBean historicEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(historicDataSource());
        bean.setJpaVendorAdapter(historicJpaVendorAdapter());
        bean.setPackagesToScan("fr.che_cse.historic");

        return bean;
    }

    @Bean(name = "historicTransactionManager")
    public JpaTransactionManager historicTransactionManager(@Qualifier("historicEntityManager") EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }
}
