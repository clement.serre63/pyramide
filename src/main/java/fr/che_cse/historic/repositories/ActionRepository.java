package fr.che_cse.historic.repositories;

import fr.che_cse.historic.models.Action;
import fr.che_cse.historic.models.Historic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ActionRepository extends JpaRepository<Action, Long> {
    @Transactional
    List<Action> findAllByHistoric(Historic historic);
}
