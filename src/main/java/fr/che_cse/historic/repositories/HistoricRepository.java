package fr.che_cse.historic.repositories;

import fr.che_cse.historic.models.Historic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoricRepository extends JpaRepository<Historic, Long> {
}
