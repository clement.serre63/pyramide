package fr.che_cse.historic.models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "historics")
public class Historic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    public Long id;

    @Column(name = "date", nullable = false, updatable = false)
    public Date date;

    @OneToMany(fetch = FetchType.EAGER)
    public List<Action> actions;

    public Historic() {
        date = new Date();
    }
}
