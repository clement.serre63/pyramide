package fr.che_cse.historic.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "actions")
public class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    @JsonIgnore
    public Long id;

    @Column(name = "date", nullable = false, updatable = false)
    public Date date;

    @ManyToOne
    @JsonIgnore
    public Historic historic;

    @Column(name = "body", nullable = false)
    public String body;

    public Action() {
        this("");
    }

    public Action(String body) {
        this.date = new Date();
        this.body = body;
    }

}
