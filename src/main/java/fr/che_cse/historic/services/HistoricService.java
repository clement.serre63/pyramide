package fr.che_cse.historic.services;

import fr.che_cse.historic.models.Action;
import fr.che_cse.historic.models.Historic;
import fr.che_cse.historic.repositories.ActionRepository;
import fr.che_cse.historic.repositories.HistoricRepository;
import fr.che_cse.historic.utils.exceptions.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Optional;

@Service
@Transactional
public class HistoricService {

    @Autowired
    private HistoricRepository historicRepository;

    @Autowired
    private ActionRepository actionRepository;

    /**
     * Create a new historic session in DB.
     *
     * @return the created historic
     */
    public Historic createSession() {
        Historic historic = new Historic();

        historicRepository.save(historic);

        return historic;
    }

    /**
     * find an historic from its id.
     *
     * @param id: id of the historic to find
     * @return the historic if found
     * @throws BadRequestException: if historic not found
     */
    public Historic findHistoric(@NotNull String id) throws BadRequestException {
        Optional<Historic> optionalHistoric = historicRepository.findById(Long.valueOf(id));

        if (!optionalHistoric.isPresent()) {
            throw new BadRequestException("Historic not found");
        }

        Historic historic = optionalHistoric.get();
        initHistoricFromDB(historic);

        return historic;
    }

    /**
     * Load actions.
     *
     * @param historic: historic from which to load actions
     */
    private void initHistoricFromDB(@NotNull Historic historic) {
        if (historic.actions == null) {
            historic.actions = new ArrayList<>();
        }
    }

    /**
     * Add a new action to the historic.
     *
     * @param historicId:
     * @param body:
     * @return the added action
     * @throws BadRequestException: if historic not found
     */
    public Action addActionToHistoric(@NotNull String historicId, @NotNull String body) throws BadRequestException {
        return addActionToHistoric(findHistoric(historicId), body);
    }

    /**
     * Add a new action to the historic.
     *
     * @param historic:
     * @param body:
     * @return the added action
     */
    public Action addActionToHistoric(@NotNull Historic historic, @NotNull String body) {
        Action action = new Action(body);

        actionRepository.save(action);

        historic.actions.add(action);
        historicRepository.save(historic);

        return action;
    }

}
