package fr.che_cse.pyramide;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "pyramideEntityManager",
        transactionManagerRef = "pyramideTransactionManager",
        basePackages = "fr.che_cse.pyramide"
)
public class PyramideDBConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.pyramide.datasource")
    public DataSource pyramideDataSource() {
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
    }

    @Bean
    public JpaVendorAdapter pyramideJpaVendorAdapter() {
        HibernateJpaVendorAdapter bean = new HibernateJpaVendorAdapter();
        bean.setDatabase(Database.H2);
        bean.setGenerateDdl(true);
        bean.setShowSql(true);

        return bean;
    }

    @Bean(name = "pyramideEntityManager")
    public LocalContainerEntityManagerFactoryBean pyramideEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(pyramideDataSource());
        bean.setJpaVendorAdapter(pyramideJpaVendorAdapter());
        bean.setPackagesToScan("fr.che_cse.pyramide");

        return bean;
    }

    @Bean(name = "pyramideTransactionManager")
    public JpaTransactionManager pyramideTransactionManager(@Qualifier("pyramideEntityManager") EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }
}
