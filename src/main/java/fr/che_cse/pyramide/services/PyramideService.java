package fr.che_cse.pyramide.services;

import fr.che_cse.pyramide.models.Card;
import fr.che_cse.pyramide.models.Game;
import fr.che_cse.pyramide.models.Move;
import fr.che_cse.pyramide.repositories.GameRepository;
import fr.che_cse.pyramide.utils.clients.DeckAPIClient;
import fr.che_cse.pyramide.utils.clients.HistoricAPIClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PyramideService {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private DeckAPIClient deckAPIClient;

    @Autowired
    private HistoricAPIClient historicAPIClient;

    public Game newGame(int nbPlayers) throws Exception {
        Game game = new Game();

        game.id = deckAPIClient.createDeck();
        deckAPIClient.shuffleDeck(game.id);

        for (int i = 0; i < nbPlayers; ++i) {
            game.players.add(historicAPIClient.createHistoric());
        }

        gameRepository.save(game);

        return game;
    }

    public Integer getScore(Long playerId) throws Exception {
        List<Move> moveList = historicAPIClient.getMoves(playerId);
        Integer score = 0;

        for (Move move : moveList) {
            score += getScore(move, moveList);
        }

        return score;
    }

    private Integer getScore(Move move, List<Move> moveList) {
        int score = 0;
        switch (move.turn) {
            case 1:
                return move.card.isSameColor(move.choice) ? 1 : 0;
            case 2:
                score += (moveList.get(0).card.value > move.card.value) && move.choice.equals("Higher") ? 1 : 0;
                score += (moveList.get(0).card.value < move.card.value) && move.choice.equals("Lower") ? 1 : 0;

                return score;
            case 3:
                if (move.choice.equals("Between")) {
                    score += (moveList.get(0).card.value > move.card.value) && (moveList.get(1).card.value < move.card.value) ? 0 : 1;
                } else {
                    score += (moveList.get(0).card.value > move.card.value) && (moveList.get(1).card.value < move.card.value) ? 1 : 0;
                }
                return score;
            default:
                return 0;
        }
    }

    public void makeTheMove(Long gameId, Integer turn, Long playerId, String choice) throws Exception {
        Card card = deckAPIClient.getCard(gameId);
        Move move = new Move(turn, choice, card);

        historicAPIClient.addMove(playerId, move);
    }

    public List<Card> getCards(Long playerId) throws Exception {
        return historicAPIClient.getCards(playerId);
    }

    public boolean gameExists(Long gameId) {
        return null != getGame(gameId);
    }

    public boolean playerExists(Long gameId, Long playerId) {
        Game game = getGame(gameId);

        if (game != null) {
            return game.players.contains(playerId);
        }

        return false;
    }

    public Game getGame(Long gameId) {
        Optional<Game> game = gameRepository.findById(gameId);

        return (game.isPresent()) ? game.get() : null;
    }
}
