package fr.che_cse.pyramide.controllers;

import fr.che_cse.pyramide.models.Game;
import fr.che_cse.pyramide.models.Player;
import fr.che_cse.pyramide.services.PyramideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PyramideController {

    @Autowired
    private PyramideService pyramideService;

    @Value("${spring.application.name}")
    String appName;

    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        model.addAttribute("users", null);

        return "home";
    }

    @PostMapping("/")
    public String home(HttpServletRequest request, Model model) throws Exception {

        Integer nbPlayers = getIntegerParameter("nbPlayers", request);

        if (nbPlayers == null || nbPlayers <= 0) {
            return "redirect:/";
        }

        Game game = pyramideService.newGame(nbPlayers);

        model.addAttribute("appName", appName);
        model.addAttribute("users", game.players);

        return "redirect:/" + game.id + "/turn1/" + game.players.get(0);
    }

    @GetMapping("/{gameId}/turn1/{playerId}")
    public String turn1Page(@PathVariable("gameId") Long gameId, @PathVariable("playerId") Long playerId, Model model) throws Exception {
        if (!pyramideService.gameExists(gameId) || !pyramideService.playerExists(gameId, playerId)) {
            return "redirect:/";
        }

        model.addAttribute("playerId", playerId);
        model.addAttribute("playerScore", pyramideService.getScore(playerId));
        model.addAttribute("playerCards", pyramideService.getCards(playerId));

        return "turn1";
    }

    @PostMapping("/{gameId}/turn1/{playerId}")
    public String turn1(HttpServletRequest request, @PathVariable("gameId") Long gameId, @PathVariable("playerId") Long playerId, Model model) throws Exception {

        String choice = request.getParameter("choice");

        pyramideService.makeTheMove(gameId, 1, playerId, choice);

        Game game = pyramideService.getGame(gameId);

        int index = game.players.indexOf(playerId);

        if (index < game.players.size() - 1) {
            return "redirect:/" + gameId + "/turn1/" + game.players.get(index + 1);
        }

        return "redirect:/" + gameId + "/turn2/" + game.players.get(0);
    }

    @GetMapping("/{gameId}/turn2/{playerId}")
    public String turn2Page(@PathVariable("gameId") Long gameId, @PathVariable("playerId") Long playerId, Model model) throws Exception {
        if (!pyramideService.gameExists(gameId) || !pyramideService.playerExists(gameId, playerId)) {
            return "redirect:/";
        }

        model.addAttribute("playerId", playerId);
        model.addAttribute("playerScore", pyramideService.getScore(playerId));
        model.addAttribute("playerCards", pyramideService.getCards(playerId));

        return "turn2";
    }

    @PostMapping("/{gameId}/turn2/{playerId}")
    public String turn2(HttpServletRequest request, @PathVariable("gameId") Long gameId, @PathVariable("playerId") Long playerId, Model model) throws Exception {

        String choice = request.getParameter("choice");

        pyramideService.makeTheMove(gameId, 2, playerId, choice);

        Game game = pyramideService.getGame(gameId);

        int index = game.players.indexOf(playerId);

        if (index < game.players.size() - 1) {
            return "redirect:/" + gameId + "/turn2/" + game.players.get(index + 1);
        }

        return "redirect:/" + gameId + "/turn3/" + game.players.get(0);
    }

    @GetMapping("/{gameId}/turn3/{playerId}")
    public String turn3Page(@PathVariable("gameId") Long gameId, @PathVariable("playerId") Long playerId, Model model) throws Exception {
        if (!pyramideService.gameExists(gameId) || !pyramideService.playerExists(gameId, playerId)) {
            return "redirect:/";
        }

        model.addAttribute("playerId", playerId);
        model.addAttribute("playerScore", pyramideService.getScore(playerId));
        model.addAttribute("playerCards", pyramideService.getCards(playerId));

        return "turn3";
    }

    @PostMapping("/{gameId}/turn3/{playerId}")
    public String turn3(HttpServletRequest request, @PathVariable("gameId") Long gameId, @PathVariable("playerId") Long playerId, Model model) throws Exception {

        String choice = request.getParameter("choice");

        pyramideService.makeTheMove(gameId, 3, playerId, choice);

        Game game = pyramideService.getGame(gameId);

        int index = game.players.indexOf(playerId);

        if (index < game.players.size() - 1) {
            return "redirect:/" + gameId + "/turn3/" + game.players.get(index + 1);
        }

        return "redirect:/" + gameId + "/result";
    }

    @GetMapping("/{gameId}/result")
    public String resultPage(@PathVariable("gameId") Long gameId, Model model) throws Exception {
        if (!pyramideService.gameExists(gameId)) {
            return "redirect:/";
        }

        List<Player> playerList = new ArrayList<>();
        Game game = pyramideService.getGame(gameId);

        for (Long playerId : game.players) {
            playerList.add(new Player(playerId, pyramideService.getScore(playerId), pyramideService.getCards(playerId)));
        }

        model.addAttribute("players", playerList);

        return "result";
    }

    @PostMapping("/{gameId}/result")
    public String quit(@PathVariable("gameId") Long gameId, Model model) {
        return "redirect:/";
    }


    private Integer getIntegerParameter(String parameter, HttpServletRequest request) {
        String value = request.getParameter(parameter);

        if (value.isEmpty()) {
            return null;
        }

        return Integer.valueOf(value);
    }
}