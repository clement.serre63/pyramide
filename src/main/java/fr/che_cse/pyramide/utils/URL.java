package fr.che_cse.pyramide.utils;

public class URL {
    private static final String HOST = "http://localhost:8085";

    public static final String HISTORIC_BASE = HOST + "/api/historics";
    public static final String HISTORIC_ACTIONS = HISTORIC_BASE + "/actions";

    public static final String DECK_BASE = HOST + "/api/decks";
    public static final String DECK_SHUFFLE = DECK_BASE + "/shuffle";
    public static final String DECK_CARDS = DECK_BASE + "/cards";
}
