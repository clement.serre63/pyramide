package fr.che_cse.pyramide.utils.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.che_cse.pyramide.models.Card;
import fr.che_cse.pyramide.models.Move;
import fr.che_cse.pyramide.utils.URL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HistoricAPIClient {
    RestTemplate restTemplate = new RestTemplate();
    ObjectMapper objectMapper = new ObjectMapper();

    public Long createHistoric() throws Exception {
        String result = restTemplate.postForObject(URL.HISTORIC_BASE, null, String.class);

        JSONObject jsonObject = new JSONObject(result);

        if (jsonObject.has("id")) {
            return jsonObject.getLong("id");
        }

        throw new Exception("No field id in returned object : " + result);
    }

    public List<Move> getMoves(Long id) throws Exception {
        String result = restTemplate.getForObject(URL.HISTORIC_BASE + "?id=" + id, String.class);

        JSONObject historic = new JSONObject(result);

        if (!historic.has("actions")) {
            throw new Exception("Unable to retrieve historic from player " + id);
        }

        JSONArray actions = historic.getJSONArray("actions");

        Iterator iterator = actions.iterator();
        List<Move> moveList = new ArrayList<>();
        while (iterator.hasNext()) {
            JSONObject jsonObject = new JSONObject(iterator.next().toString());
            moveList.add(objectMapper.readValue(jsonObject.getString("body"), Move.class));
        }

        return moveList;
    }

    public void addMove(Long id, Move move) throws Exception {
        String body = objectMapper.writeValueAsString(move);
        restTemplate.postForObject(URL.HISTORIC_ACTIONS + "?id=" + id, body, String.class);
    }

    public List<Card> getCards(Long id) throws Exception {
        List<Move> moveList = getMoves(id);
        List<Card> cardList = new ArrayList<>();

        for (Move move : moveList) {
            cardList.add(move.card);
        }

        return cardList;
    }
}
