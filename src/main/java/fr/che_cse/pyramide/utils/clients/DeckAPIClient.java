package fr.che_cse.pyramide.utils.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.che_cse.pyramide.models.Card;
import fr.che_cse.pyramide.utils.URL;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class DeckAPIClient {
    RestTemplate restTemplate = new RestTemplate();
    ObjectMapper objectMapper = new ObjectMapper();

    public Long createDeck() throws Exception {
        String result = restTemplate.postForObject(URL.DECK_BASE, null, String.class);

        JSONObject jsonObject = new JSONObject(result);

        if (jsonObject.has("id")) {
            return jsonObject.getLong("id");
        }

        throw new Exception("No field id in returned object : " + result);
    }

    public void shuffleDeck(Long id) throws RestClientException {
        restTemplate.postForObject(URL.DECK_SHUFFLE + "?id=" + id, null, String.class);
    }

    public Card getCard(Long id) throws Exception {
        String result = restTemplate.postForObject(URL.DECK_CARDS + "?id=" + id, null, String.class);

        return objectMapper.readValue(result, Card.class);
    }
}
