package fr.che_cse.pyramide.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "games")
public class Game {

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    public Long id;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "players", joinColumns = @JoinColumn(name = "game_id"))
    @Column(name = "player_id")
    public List<Long> players = new ArrayList<>();

    public Game() {
    }
}
