package fr.che_cse.pyramide.models;

public class Card {
    public String color;
    public Integer value;

    public boolean isSameColor(String color) {
        return ((color.equals("Red") && (this.color.equals("pique") || this.color.equals("trefle"))) ||
                (color.equals("Black") && (this.color.equals("carreau") || this.color.equals("coeur"))));
    }
}
