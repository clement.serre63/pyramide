package fr.che_cse.pyramide.models;

import java.util.List;

public class Player {
    public Long id;
    public Integer score;
    public List<Card> cards;

    public Player(Long id, Integer score, List<Card> cards) {
        this.id = id;
        this.score = score;
        this.cards = cards;
    }
}
