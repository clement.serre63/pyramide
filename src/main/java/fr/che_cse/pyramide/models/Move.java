package fr.che_cse.pyramide.models;

public class Move {
    public Integer turn;
    public String choice;
    public Card card;

    public Move() {
    }

    public Move(Integer turn, String choice, Card card) {
        this.turn = turn;
        this.choice = choice;
        this.card = card;
    }
}
