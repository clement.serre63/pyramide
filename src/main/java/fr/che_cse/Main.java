package fr.che_cse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {
    static public void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
